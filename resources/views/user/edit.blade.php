@extends('layouts.app')

@section('title', 'Usuarios')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">

      <h1>Editar usuario</h1>

      <form method="post" action="/users/{{ $user->id }}">
        {{ csrf_field() }}

        <input type="hidden" name="_method" value="PUT">


        <div class="form-group row {{ $errors->has('name') ? ' has-error' : '' }}">
          <label for="name" class="col-md-4  col-form-label text-md-right">
            Nombre
          </label>

          <div class="col-md-8">
            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"  name="name" value="{{ old('name') ? old('name') : $user->name }}" {{--required--}} autofocus>

            @if ($errors->has('name'))
            <span class="help-block">
              <strong>{{ $errors->first('name') }}</strong>
            </span>
            @endif
          </div>
        </div>



        <div class="form-group row {{ $errors->has('email') ? ' has-error' : '' }}">
          <label for="email" class="col-md-4  col-form-label text-md-right">
            Email
          </label>

          <div class="col-md-8">
            <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"  name="email" value="{{ old('email') ? old('email') : $user->email }}" {{--required--}} autofocus>

            @if ($errors->has('email'))
            <span class="help-block">
              <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
          </div>
        </div>


        <div class="form-group row mb-0">
          <div class="col-md-6 offset-md-4">
            <button type="submit" class="btn btn-primary">
              {{-- __('New')--}}
              @lang('Update')
            </button>
          </div>
        </div>

      </form>

    </div>
  </div>
</div>

@endsection
